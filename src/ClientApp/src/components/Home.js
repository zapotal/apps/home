import React, { Component } from 'react';

export class Home extends Component {
    static displayName = Home.name;

    constructor(props) {
        super(props);
        this.state = {
            subdomain: () => {
                var url = window.location.href;
                //var url = "https://dev.zapotal.club/";
                if (url.indexOf("dev.") > -1) {
                    return "dev.";
                }
                return "";
            }
        };
    }

  render () {
    return (
      <div>
            <h1>Hello world! </h1>
            <br />
            <p>This is a personal project design to showcase my DevOps skills to the world!
                <br />If you want to check the code and read more about it please go to my <a href="https://gitlab.com/zapotal/infra/gke">GitLab</a> repository.
            </p>
            <p>In the meantime please check the different sites available in Zapotal!</p>
            <ul>
                <li><strong><a href={"https://" + this.state.subdomain() + "blog.zapotal.club"} target="_blank">Zapotal Blog</a></strong>: This is a blog based on Ghost, which is a free and open source blogging platform written in JavaScript and distributed under the MIT License, designed to simplify the process of online publishing for individual bloggers as well as online publications.</li><br/>
                <li><strong><a href={"https://" + this.state.subdomain() + "money.zapotal.club"} target="_blank">Zapotal Money</a></strong>: Free currency converter to calculate exchange rates for currencies. Built using react webhooks.</li><br />
                <li><strong><a href={"https://" + this.state.subdomain() + "zapotal.club"} target="_blank">Zapotal Home</a></strong>: Zapotal home page where you will find all available zapotal websites.</li>
            </ul>
            <br/>
            <p>Develop by Brandon Alvarez (<a href="mailto:brandon.alvarez.cr@gmail.com">brandon.alvarez.cr@gmail.com</a>).</p>
      </div>
    );
  }
}
